#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
   const int n = 1<<20;
   int i;
   int r = 0;

#pragma omp parallel default(shared) 
{
#pragma omp for reduction(+:r) shared(i)
   for (i=0; i<n; ++i) {
      r += 1;
   }
#pragma omp critical
   {
   printf("%d\n", r);
   }
}

   return 0;
}
