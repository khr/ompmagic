# ompmagic.h

C header file to enable independent switching of OpenMP parallel and OpenMP SIMD
regions.  Most compilers do not allow to enable OpenMP parallel and to disable
OpenMP SIMD at the same time.  This is useful when debugging threaded and
vectorized code.

Simply define e.g.
```
#define USE_OMP_THREADS 0
#define USE_OMP_VECTORS 1
```
and include the header file while compiling with OpenMP enabled (GCC: `-fopenmp`).

See `loop.c` for a minimal usage example.

