CC=gcc
# -O3 activates the auto-vectorizer
CFLAGS+=-g -O2 -march=native
CFLAGS+=-fopt-info
CFLAGS+=-fopenmp
#CFLAGS+=-fopenmp-simd
#CFLAGS+=-fno-openmp-simd


#CC=icc
# -O2 activates the auto-vectorizer :(
#CFLAGS+=-g -O2 -xHost
#CFLAGS+=-qopenmp
#CFLAGS+=-qopenmp-simd
#CFLAGS+=-qno-openmp-simd


loop: loop.c Makefile
	$(CC) $(CFLAGS) -o $@ $<
	objdump -S $@ | grep vaddpd || true

.PHONY: clean
clean:
	rm -f loop

