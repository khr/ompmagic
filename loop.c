/**
 * Klaus Reuter, khr@rzg.mpg.de
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// switches must be defined before the inclusion of the header file
#define USE_OMP_THREADS 1
#define USE_OMP_VECTORS 1
#include "ompmagic.h"

int main() {
   const int n = 1<<20;
   double *a;
   double *b;
   double *c;
   int i;
   int n_bytes;

   n_bytes = n * sizeof(double);
   a = (double*) malloc(n_bytes);
   b = (double*) malloc(n_bytes);
   c = (double*) malloc(n_bytes);
   memset(a, 0, n_bytes);
   memset(b, 0, n_bytes);
   memset(c, 0, n_bytes);

   PRAGMA_OMP_PARALLEL( default(shared) )
   PRAGMA_OMP_FOR()
   for (i=0; i<n; ++i) {
      a[i] = (double)i;
      b[i] = (double)i;
   }

   PRAGMA_OMP_SIMD()
   for (i=0; i<n; ++i) {
      c[i] = a[i] + b[i];
   }

   PRAGMA_OMP_PARALLEL( default(shared) )
   PRAGMA_OMP_FOR_SIMD()
   for (i=0; i<n; ++i) {
      c[i] = a[i] + b[i];
   }

   printf("%f\n", c[n-1]);

   free(a);
   free(b);
   free(c);
   return 0;
}
